﻿#pragma once
#include <windows.h>
#include <math.h>
#include "changeable.h"

void resize(int k, int b) {
	HWND console = GetConsoleWindow();
	RECT r;
	GetWindowRect(console, &r);
	MoveWindow(console, r.left, r.top, k, b, TRUE);
}

void printNumbering() {
	printf("   ");
	for (int i = 1; i <= 11; i++) {
		printf("%-13d", i);
	}
	printf("\n");
}

void printDoubleArray(double* array, char* name, int sizeX, int sizeY) {
	printf("%s\n",name);
	printNumbering();
	int stepx = (sizeX - 1) / 10;
	int stepy = (sizeY - 1) / 10;
	for (int k = 0; k < 11; k++) {
		printf("%-2d|", k+1);
		for (int i = 0; i < 11; i++) {
			printf("%-*g|", 12, *(array + i*stepx + (k*stepy) * sizeX));
		}
		printf("\n");
	}
}

void printTopArray(double* anal, double* cal,char* name, int sizeX, int sizeY, double h) {
	printf("\n%s\n", name);
	printf("        ");
	printNumbering();
	int stepx = (sizeX - 1) / 10;
	//int stepy = (sizeY - 1) / 10;
	printf("%-10s|", "Numeric");
	for (int i = 0; i < 11; i++) {
		printf("%-*g|", 12, *(cal + i*stepx + (sizeY-1) * sizeX));
	}
	printf("\n");
	printf("%-10s|", "Analitic");
	for (int i = 0; i < 11; i++) {
		printf("%-*g|", 12, *(anal + i + 10 * 11));
	}
	printf("\n");
	printf("%-10s|", "Diff");
	double maxMod=0.0;
	for (int i = 0; i < 11; i++) {
		double diff = *(anal + i + 10 * 11) - *(cal + i*stepx + (sizeY-1) * sizeX);
		printf("%-*g|", 12, diff);
		if (maxMod < fabs(diff)) maxMod = fabs(diff);
	}
	for (int i = 0; i < sizeX; i++) {
		double diff = analSolution(i*h, 1.0) - *(cal + i + (sizeY - 1) * sizeX);
		if (maxMod < fabs(diff)) maxMod = fabs(diff);
	}
	printf("\n--->MaxDiff:%g\n", maxMod);
}

void printDoubleArrayDiff(double* array,double* array1, char* name, int sizeX, int sizeY) {
	printf("%s\n", name);
	printNumbering();
	int stepx = (sizeX - 1) / 10;
	int stepy = (sizeY - 1) / 10;
	for (int k = 0; k < 11; k++) {
		printf("%-2d|", k + 1);
		for (int i = 0; i < 11; i++) {
			printf("%-*g|", 12, *(array + i*stepx + (k*stepy) * sizeX)-*(array1 + i*stepx + (k*stepy) * sizeX));
		}
		printf("\n");
	}
}

void printFullDoubleArray(double* array, char* name, int sizeX, int sizeY) {
	printf("%s\n", name);
	printNumbering();
	for (int k = 0; k < sizeY; k++) {
		printf("%-2d|", k + 1);
		for (int i = 0; i < sizeX; i++) {
			printf("%-*g|", 12, *(array + i + (k) * sizeX));
		}
		printf("\n");
	}
}

void printFullDoubleArrayInTxt(double* array, char* name, int sizeX, int sizeY, double h) {
	FILE *file = fopen("text.txt", "w");
	for (int k = 0; k < sizeY; k++) {
		fprintf(file, "%-2d|", k + 1);
		for (int i = 0; i < sizeX; i++) {
			if (k == sizeY - 1) {
				fprintf(file, "%-*g|", 12, *(array + i + (k)* sizeX));
			}
			else {
				fprintf(file, "%-*g|", 12, *(array + i + (k)* sizeX));
			}
		}
		fprintf(file,"\n");
	}
	fclose(file);
}

//++//
double ulRight(double t) {
	double result = psiDerivatives(t,0);
	return result;
}

//++//
double unBottom(double h, int l) {
	double x = h*l;
	double result = fiDerivatives(x, 0);
	return result;
}

//++//Вычисление Ul(n+1), при известных Uln, U(l+1)n, U(l+2)n, U(l+3)n
void findTopU(int n1, int l, double* doubleArray, int sizex, int sizey, double tau, double h) {
	double result, localTau = tau;
	double unl, unl1, unl2, unl3;
	unl = *(doubleArray + sizex*(n1 - 1) + l);
	unl1 = *(doubleArray + sizex*(n1 - 1) + l + 1 * right);
	unl2 = *(doubleArray + sizex*(n1 - 1) + l + 2 * right);
	unl3 = *(doubleArray + sizex*(n1 - 1) + l + 3 * right);
	if (n1 == sizey - 1) {
		localTau = fabsf(((double)(sizey - 2))*tau - 1.0);
		//printf("LocalTau: %g\n", localTau);
	}
	result = unl*unlCoeff(localTau, h, n1 - 1, l) +
		unl1*unl1Coeff(localTau, h, n1 - 1, l) +
		unl2*unl2Coeff(localTau, h, n1 - 1, l) +
		unl3*unl3Coeff(localTau, h, n1 - 1, l) +
		aCoef(localTau, h, n1 - 1, l);
		*(doubleArray + n1*sizex + l) = result;
}

//++//Заполнение правого или левого столбца матрицы решений
void fillRightEdge(double* doubleArray, int sizex, int sizey, double tau) {
	if (right == 1) {
		for (int i = 0; i < sizey; i++) {
			if (i == sizey - 1) {
				*(doubleArray + i*sizex + sizex - 1) = ulRight(1.0);
			}
			else {
				*(doubleArray + i*sizex + sizex - 1) = ulRight(i*tau);
			}	
		}
	}
	if (right == -1) {
		for (int i = 0; i < sizey; i++) {
			if (i == sizey - 1) {
				*(doubleArray + i*sizex) = ulRight(1.0);
			}
			else {
				*(doubleArray + i*sizex) = ulRight(i*tau);
			}
		}
	}
}

//След первой производной на границе
double firstUDerivative(double t) {
	double a, b, psi1;
	if (right == 1) {
		a = aDerivatives(rightBorder, t, 0, 0);
		b = bDerivatives(rightBorder, t, 0, 0);
		psi1 = psiDerivatives(t, 1);
	} else {
		a = aDerivatives(leftBorder, t, 0, 0);
		b = bDerivatives(leftBorder, t, 0, 0);
		psi1 = psiDerivatives(t, 1);
	}
	double result = (1.0 / a)*(-psi1 + b);
	return result;
}

double secondUDerivative(double t) {
	double a, a1x, a1t, b, b1x, b1t, psi1, psi2, result;
	if (right == 1) {
		a = aDerivatives(rightBorder, t, 0, 0);
		b = bDerivatives(rightBorder, t, 0, 0);
		a1x = aDerivatives(rightBorder, t, 1, 1);
		a1t = aDerivatives(rightBorder, t, 1, 0);
		b1x = bDerivatives(rightBorder, t, 1, 1);
		b1t = bDerivatives(rightBorder, t, 1, 0);
		psi1 = psiDerivatives(t, 1);
		psi2 = psiDerivatives(t, 2);
	}
	else {
		a = aDerivatives(leftBorder, t, 0, 0);
		b = bDerivatives(leftBorder, t, 0, 0);
		a1x = aDerivatives(leftBorder, t, 1, 1);
		a1t = aDerivatives(leftBorder, t, 1, 0);
		b1x = bDerivatives(leftBorder, t, 1, 1);
		b1t = bDerivatives(leftBorder, t, 1, 0);
		psi1 = psiDerivatives(t, 1);
		psi2 = psiDerivatives(t, 2);
	}
	result = (1 / pow(a, 2.0))*(psi2 + (a1t - a*a1x)*(1 / a)*(b - psi1) - b1t + a*b1x);
	return result;
}

double thirdUDerivative(double t) {
	double a, a1x, a1t, a2xx, a2xt, a2tt, b, b1x, b1t, b2xx, b2tt, b2xt, psi3, result;
	if (right == 1) {
		a = aDerivatives(rightBorder, t, 0, 0);
		b = bDerivatives(rightBorder, t, 0, 0);
		a1x = aDerivatives(rightBorder, t, 1, 1);
		a1t = aDerivatives(rightBorder, t, 1, 0);
		b1x = bDerivatives(rightBorder, t, 1, 1);
		b1t = bDerivatives(rightBorder, t, 1, 0);
		a2tt = aDerivatives(rightBorder, t, 2, 0);
		a2xx = aDerivatives(rightBorder, t, 2, 1);
		a2xt = aDerivatives(rightBorder, t, 2, 2);
		b2tt = bDerivatives(rightBorder, t, 2, 0);
		b2xx = bDerivatives(rightBorder, t, 2, 1);
		b2xt = bDerivatives(rightBorder, t, 2, 2);
		psi3 = psiDerivatives(t, 3);
	}
	else {
		a = aDerivatives(leftBorder, t, 0, 0);
		b = bDerivatives(leftBorder, t, 0, 0);
		a1x = aDerivatives(leftBorder, t, 1, 1);
		a1t = aDerivatives(leftBorder, t, 1, 0);
		b1x = bDerivatives(leftBorder, t, 1, 1);
		b1t = bDerivatives(leftBorder, t, 1, 0);
		a2tt = aDerivatives(leftBorder, t, 2, 0);
		a2xx = aDerivatives(leftBorder, t, 2, 1);
		a2xt = aDerivatives(leftBorder, t, 2, 2);
		b2tt = bDerivatives(leftBorder, t, 2, 0);
		b2xx = bDerivatives(leftBorder, t, 2, 1);
		b2xt = bDerivatives(leftBorder, t, 2, 2);
		psi3 = psiDerivatives(t, 3);
	}
	result = ((-1.0) / pow(a, 3.0))*psi3 +
		(3.0 / pow(a, 2.0))*(a1t - a*a1x)*secondUDerivative(t) +
		((-1.0) / pow(a, 3.0))*(a2tt - a*a2xt + a*a*a2xx - 2.0*a1t*a1x + a*a1x*a1x)*firstUDerivative(t) +
		((1.0) / pow(a, 3.0))*(b2tt - a*b2xt + a*a*b2xx - 2.0*a1t*b1x + a*a1x*b1x);
	return result;
}

//++//
void fillRightCorner1(double* mass, double tau, double h, int sizex, int sizey) {
	int n = sizex-2;
	double x = rightBorder;
	if (right == -1) {
		n = 1;
		x = leftBorder;
	}
	for (int k = 1; k < sizey; k++) {
		if (k == sizey - 1) {
			*(mass + k*sizex + n) = psiDerivatives(1.0, 0) +
				h*(-right)*firstUDerivative(1.0) +
				(pow(h*(-right), 2.0) / 2.0)*secondUDerivative(1.0) +
				(pow(h*(-right), 3.0) / 6.0)*thirdUDerivative(1.0);
		}
		else {
			*(mass + k*sizex + n) = psiDerivatives(tau*k, 0) +
				h*(-right)*firstUDerivative(tau*k) +
				(pow(h*(-right), 2.0) / 2.0)*secondUDerivative(tau*k) +
				(pow(h*(-right), 3.0) / 6.0)*thirdUDerivative(tau*k);
		}
	}
}

//++//
void fillRightCorner2(double* mass, double tau, double h, int sizex, int sizey) {
	int n = sizex - 3;
	double x = rightBorder;
	if (right == -1) {
		n = 2;
		x = leftBorder;
	}
	for (int k = 1; k < sizey; k++) {
		if (k == sizey - 1) {
			*(mass + k*sizex + n) = psiDerivatives(1.0, 0) +
				2 * h*(-right)*firstUDerivative(1.0) +
				(pow(2 * h*(-right), 2.0) / 2.0)*secondUDerivative(1.0) +
				(pow(2 * h*(-right), 3.0) / 6.0)*thirdUDerivative(1.0);
		}
		else {
			*(mass + k*sizex + n) = psiDerivatives(tau*k, 0) +
				2 * h*(-right)*firstUDerivative(tau*k) +
				(pow(2 * h*(-right), 2.0) / 2.0)*secondUDerivative(tau*k) +
				(pow(2 * h*(-right), 3.0) / 6.0)*thirdUDerivative(tau*k);
		}
	}
}

//++//Заполнение первой строки матрицы решений
void fillBottom(double* mass, double h, double tau, int sizex) {
	for (int k = 0; k < sizex; k++) {
		*(mass + k) = unBottom(h, k);
	}
}

void fillU(double* mass, double h, double tau, int sizex, int sizey) {
	fillBottom(mass, h, tau, sizex);
	fillRightEdge(mass, sizex, sizey, tau);
	fillRightCorner1(mass, tau, h, sizex, sizey);
	fillRightCorner2(mass, tau, h, sizex, sizey);
	if (right == 1) {
		for (int l = sizex - 4; l >= 0; l--) {
			for (int n = 1; n < sizey; n++) {
				findTopU(n, l, mass, sizex, sizey, tau, h);
			}
		}
	}
	if (right == -1) {
		for (int l = 3; l < sizex; l++) {
			for (int n = 1; n < sizey; n++) {
				findTopU(n, l, mass, sizex, sizey, tau, h);
			}
		}
	}
}
