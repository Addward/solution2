#pragma once
#include <math.h>

int right = 1;
double rightBorder = 1.0;
double leftBorder = 0.0;

double analSolution(double x, double t) {
	return sin(x + 4 * t) - x*x*0.125;
}
//++//
double fiDerivatives(double x, int order) {
	if (order == 0) {
		return sin(x) - 0.125 * x * x;
	}
	if (order == 1) {
		return cos(x) - 0.125 * 2.0 * x;
	}
	if (order == 2) {
		return -sin(x) - 0.125 * 2.0;
	}
	return 0.0;
}

//++//
double psiDerivatives(double t, int order) {
	if (order == 0) {
		return sin(1 + 4 * t) - 0.125;
	}
	if (order == 1) {
		return cos(1 + 4 * t) * 4.0;
	}
	if (order == 2) {
		return -sin(1 + 4 * t) * 4.0 * 4.0;
	}
	if (order == 3) {
		return -cos(1 + 4 * t) * 4.0 * 4.0 * 4.0;
	}
	return 0.0;
}

//++//
double aDerivatives(double x, double t, int order, int type) {
	if (order == 0) {
		return (-4.0);
	}
	if (order == 1) {
		// ����������� �� t
		if (type == 0) return (0.0);
		// ����������� �� x
		if (type == 1) return (0.0);
	}
	if (order == 2) {
		// ����������� �� t
		if (type == 0) return (0.0);
		// ����������� �� x
		if (type == 1) return (0.0);
		// ��������� �����������
		if (type == 2) return (0.0);
	}
	return 0.0;
}

//++//
double bDerivatives(double x, double t, int order, int type) {
	if (order == 0) {
		return x;
	}
	if (order == 1) {
		// ����������� �� t
		if (type == 0) return (0.0);
		// ����������� �� x
		if (type == 1) return (1.0);
	}
	if (order == 2) {
		// ����������� �� t
		if (type == 0) return (0.0);
		// ����������� �� x
		if (type == 1) return (0.0);
		// ��������� �����������
		if (type == 2) return (0.0);
	}
	return 0;
}

double unlCoeff(double tau, double h, int n, int l) {
	double result = 1.0 + ((-22.0) * tau) / (3.0 * h) + 16.0*pow(tau / h, 2.0) + (-32.0 / 3.0)*pow(tau / h, 3.0);
	return result;
}

double unl1Coeff(double tau, double h, int n, int l) {
	double result = 12.0*tau / h + (-40.0)*pow(tau / h, 2.0) + (32.0)*pow(tau / h, 3.0);
	return result;
}

double unl2Coeff(double tau, double h, int n, int l) {
	double result = (-6.0)*tau / h + 32.0*pow(tau / h, 2.0) + (-32.0)*pow(tau / h, 3.0);
	return result;
}

double unl3Coeff(double tau, double h, int n, int l) {
	double result = (4.0 / 3.0)*tau / h + (-8.0)*pow(tau / h, 2.0) + (32.0 / 3.0)*pow(tau / h, 3.0);
	return result;
}

double aCoef(double tau, double h, int n, int l) {
	double x = l * h;
	double result = tau*x + 2.0*tau*tau;
	return result;
}