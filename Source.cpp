#include <windows.h>
#include <stdio.h>
#include <malloc.h>
#include "Header.h"

int main() {
	int width,height;
	resize(1280, 600);

	double epsilon = 0.00001;
	int sizex=21, sizey=81;
	double tau = 1.0 / (double)(sizey-1);
	double h = 1.0 / (double)(sizex-1);

	double* mass = (double*)malloc(sizex*sizey * sizeof(double));
	double* sol = (double*)malloc(11 * 11*sizeof(double));
	for (int i = 0; i < 11; i++) {
		for (int k = 0; k < 11; k++) {
			*(sol + i * 11 + k) = analSolution(k*0.1,i*0.1);
		}
	}
	printDoubleArray(sol, "AnalSolution", 11, 11);
	fillU(mass, h, tau, sizex, sizey);
	
	while (true) {
		printf("Lmax:");
		scanf_s("%d", &sizex);
		if (sizex == 0) break;
		printf("Tau/h:");

		double tautoh;
		scanf_s("%lf", &tautoh);

		double h = 1.0 / (double)(sizex - 1);
		double tau = h*tautoh;
		
		double ostatok = fmod(1.0, tau);

		sizey = (int)(1.0 / tau) + 1;
		if (fabsf(((double)(sizey - 2))*tau - 1.0) > tau) sizey+=1;

		printf("sizey: %d, tau: %g", sizey, tau);

		double* mass1 = (double*)malloc(sizex*sizey * sizeof(double));
		fillU(mass1, h, tau, sizex, sizey);
	
		printTopArray(sol, mass1, "Table", sizex, sizey, h);
		//printFullDoubleArrayInTxt(mass1, "Table", sizex, sizey, h);
		free(mass1);
	}
}